#!/bin/bash

#######################################
## hive_creator
#######################################

version="v.8.0.0";

init() {
    startCounter=$(date +%s)
    clear

    infoMsg ":: HIVE | creator | $version\n" \
    "::"
    nbsp

    # get basename by reponame
    repoName=$(git config --local remote.origin.url|sed -n 's#.*/\([^.]*\)\.git#\1#p');
    suffix="_creator";
    baseName=${repoName%$suffix};

    if [ "$baseName" == "hive" ]; then
        warnMsg ":: Warning [wfc43g4287vnv4kt1379597w526z0d4d]\n" \
        ":: You could possibly use the creator from the hive\n" \
        ":: However, a fork should be used"
        nbsp
        sleep 1
    fi

    if [ ! -f "creator.example.ini" ]; then
        errorMsg ":: Error [g3978p78jzk312a0el4v1nd4ut6tb3z8] in configuration\n" \
        ":: File <creator.example.ini> does not exist\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ ! -f "creator.ini" ]; then
        logMsg "copy creator.example.ini to creator.ini"
        cp creator.example.ini creator.ini
    fi

    # include config to create a new project
    . creator.ini

    projectName=$projectname
    repoDocker=$repodocker
    repoInstaller=$repoinstaller
    repoBoilerplate=$repoboilerplate
    repoThm=$repothm
    repoDeploy=$repodeploy
    gitProjecturl=$gitprojecturl

    # use basename if projectname isn't set
    if [ "$projectName" == "projectfoldername" ]; then
        logMsg "set projectname to creator.ini"
        sed -ie "s/projectfoldername/$baseName/" creator.ini
        infoMsg "::\n" \
        ":: diff creator.inie creator.ini\n" \
        "::"
        nbsp
        infoMsg "-+"
        diff -by -W100 --suppress-common-lines ./creator.inie ./creator.ini
        infoMsg "-+"
        nbsp

        projectName=$baseName;

        rm -rf creator.inie
    fi

    # check if projectname isn't have
    if [ "$projectName" == "hive" ]; then
        errorMsg ":: Error [t8bdx6e1ev49x3ah26t3659e27587grx] in configuration\n" \
        ":: Projectname must not be hive or projectname\n" \
        ":: Please use only forks for your project\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    # set reponames if default is set
    if [ "$projectName" == "projectfoldername" ]; then
        logMsg "set projectname to creator.ini"
        sed -ie "s/projectfoldername/$projectName/" creator.ini
        infoMsg "::\n" \
        ":: diff creator.inie creator.ini\n" \
        "::"
        nbsp
        infoMsg "-+"
        diff -by -W100 --suppress-common-lines ./creator.inie ./creator.ini
        infoMsg "-+"
        nbsp

        rm -rf creator.inie
    fi

    if [ "$repoDocker" == "XXX_YY_xxx_docker" ]; then
        repoDocker=$projectName"_docker";

        logMsg "set repodocker to creator.ini"
        sed -ie "s/XXX_YY_xxx_docker/$repoDocker/" creator.ini
        infoMsg "::\n" \
        ":: diff creator.inie creator.ini\n" \
        "::"
        nbsp
        infoMsg "-+"
        diff -by -W100 --suppress-common-lines ./creator.inie ./creator.ini
        infoMsg "-+"
        nbsp

        rm -rf creator.inie
    fi

    if [ "$repoInstaller" == "XXX_YY_xxx_installer" ]; then
        repoInstaller=$projectName"_installer";

        logMsg "set repoinstaller to creator.ini"
        sed -ie "s/XXX_YY_xxx_installer/$repoInstaller/" creator.ini
        infoMsg "::\n" \
        ":: diff creator.inie creator.ini\n" \
        "::"
        nbsp
        infoMsg "-+"
        diff -by -W100 --suppress-common-lines ./creator.inie ./creator.ini
        infoMsg "-+"
        nbsp

        rm -rf creator.inie
    fi

    if [ "$repoboilerplate" == "XXX_YY_xxx_boilerplate" ]; then
        repoBoilerplate=$projectName"_boilerplate";

        logMsg "set repoboilerplate to creator.ini"
        sed -ie "s/XXX_YY_xxx_boilerplate/$repoBoilerplate/" creator.ini
        infoMsg "::\n" \
        ":: diff creator.inie creator.ini\n" \
        "::"
        nbsp
        infoMsg "-+"
        diff -by -W100 --suppress-common-lines ./creator.inie ./creator.ini
        infoMsg "-+"
        nbsp

        rm -rf creator.inie
    fi

    if [ "$repothm" == "XXX_YY_xxx_thm" ]; then
        repoThm=$projectName"_thm";

        logMsg "set repothm to creator.ini"
        sed -ie "s/XXX_YY_xxx_thm/$repoThm/" creator.ini
        infoMsg "::\n" \
        ":: diff creator.inie creator.ini\n" \
        "::"
        nbsp
        infoMsg "-+"
        diff -by -W100 --suppress-common-lines ./creator.inie ./creator.ini
        infoMsg "-+"
        nbsp

        rm -rf creator.inie
    fi

    if [ "$repodeploy" == "XXX_YY_xxx_deploy" ]; then
        repoDeploy=$projectName"_thm";

        logMsg "set repodeploy to creator.ini"
        sed -ie "s/XXX_YY_xxx_deploy/$repoDeploy/" creator.ini
        infoMsg "::\n" \
        ":: diff creator.inie creator.ini\n" \
        "::"
        nbsp
        infoMsg "-+"
        diff -by -W100 --suppress-common-lines ./creator.inie ./creator.ini
        infoMsg "-+"
        nbsp

        rm -rf creator.inie
    fi

    # include config again to get all variable changes
    . creator.ini

    projectName=$projectname
    repoDocker=$repodocker
    repoInstaller=$repoinstaller
    repoBoilerplate=$repoboilerplate
    repoThm=$repothm
    repoDeploy=$repodeploy
    gitProjecturl=$gitprojecturl

    gitfile=".git";
    infoMsg "::\n" \
    ":: Overview:\n" \
    "::\n" \
    ":: Project Name: $(tput setaf 6)$projectname$(tput sgr 0)\n" \
    "::\n" \
    ":: Git Project Base-URL: $(tput setaf 6)$gitProjecturl$(tput sgr 0)\n" \
    "::\n" \
    ":: Docker Repository: $(tput setaf 6)$repodocker$gitfile$(tput sgr 0)\n" \
    ":: Installer Repository: $(tput setaf 6)$repoinstaller$gitfile$(tput sgr 0)\n" \
    ":: Boilerplate Repository: $(tput setaf 6)$repoboilerplate$gitfile$(tput sgr 0)\n" \
    ":: Thm Repository: $(tput setaf 6)$repothm$gitfile$(tput sgr 0)\n" \
    ":: Deploy Repository: $(tput setaf 6)$repodeploy$gitfile$(tput sgr 0)\n" \
    "::\n"


    while true; do
        infoMsg "::\n" \
        ":: Are these values correct? $(tput setaf 6)y/n$(tput sgr 0)"
        read yn
        case $yn in
            [Yy]* ) make install; break;;
            [Nn]* ) exit;;
            * ) echo -e "\n:: Please answer yes or no.\n::\n";;
        esac
    done
    nbsp


    logMsg "set $projectName to .gitignore"
    sed -ie "s/projectname/$projectName/" .gitignore
    infoMsg "::\n" \
    ":: diff .gitignoree .gitignore\n" \
    "::"
    nbsp

    rm -rf .gitignoree

    # create projectfolder
    logMsg "create $projectName folder"
    mkdir $projectName

    # go into projectfolder
    cd $projectName
    nbsp

    # make shure .git is removed
    rm -rf .git

    # clone hive_docker fork
    cloneUrl=${gitProjecturl}${repoDocker}${gitfile};
    logMsg "start cloning ${cloneUrl} to hive_docker"
    git init
    git clone ${cloneUrl} hive_docker
    sleep 1
    nbsp

    if [ "$repoInstaller" == "XXX_YY_xxx_installer" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repoinstaller must not be '$repoInstaller'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$repoInstaller" == "hive_installer_typo3" ]; then
        errorMsg ":: Error [z1346r85f99w78280p59922xvb33a3ik] in configuration\n" \
        ":: \$repoinstaller must not be '$repoInstaller'\n" \
        ":: Please use only forks for your project\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    # make shure .git is removed
    rm -rf .git

    # clone hive_installer fork
    cloneUrl=${gitProjecturl}${repoInstaller}${gitfile};
    logMsg "start cloning ${cloneUrl} to hive_installer"
    git init
    git clone ${cloneUrl} hive_installer
    sleep 1
    nbsp

    if [ "$repoBoilerplate" == "XXX_YY_xxx_boilerplate" ]; then
        errorMsg ":: Error [dz67w45uqa1562czv25423r46w99k2te] in configuration\n" \
        ":: \$repoboilerplate must not be '$repoBoilerplate'\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    if [ "$repoBoilerplate" == "hive_php_boilerplate_df" ]; then
        errorMsg ":: Error [z1346r85f99w78280p59922xvb33a3ik] in configuration\n" \
        ":: \$repoboilerplate must not be '$repoBoilerplate'\n" \
        ":: Please use only forks for your project\n" \
        ":: Feel free to read the manual by executing 'make help'"
        exit
    fi

    # make shure .git is removed
    rm -rf .git

    # clone hive_boilerplate fork
    time=$(date +"%Y-%m-%d_%H-%M-%S")
    cloneUrl=${gitProjecturl}${repoBoilerplate}${gitfile};
    logMsg "start cloning ${cloneUrl} to hive_boilerplate_${time}"
    git init

    echo "git clone $cloneUrl hive_boilerplate_${time}"
    git clone $cloneUrl hive_boilerplate_${time}
    sleep 1
    nbsp

    # remove created .git
    rm -rf .git


    ###### copy forked repo variables to hive_installer ######
    cd hive_installer
    logMsg "set forked repo variables to hive_installer/install.ini"
    sed -ie "s/XXX_YY_xxx_docker/$repoDocker/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    sed -ie "s/XXX_YY_xxx_installer/$repoInstaller/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    sed -ie "s/XXX_YY_xxx_boilerplate/$repoBoilerplate/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    sed -ie "s/XXX_YY_xxx_thm/$repoThm/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    sed -ie "s/XXX_YY_xxx_deploy/$repoDeploy/" install.ini
    infoMsg "::\n" \
    ":: diff install.inie install.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.inie ./install.ini
    infoMsg "-+"
    nbsp

    cp -rpv install.local.example.ini install.local.ini
    sed -ie "s/hive_boilerplate_YY-mm-dd_HH-MM-SS/hive_boilerplate_$time/" install.local.ini
    infoMsg "::\n" \
    ":: diff install.local.inie install.local.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./install.local.inie ./install.local.ini
    infoMsg "-+"
    nbsp

    rm -rf install.inie
    rm -rf install.local.inie

    # go to hive_docker
    cd ../hive_docker

    sync="-sync"

    sed -ie "s/hive_boilerplate_YY-mm-dd_HH-MM-SS-sync/hive_boilerplate_$time$sync/" docker.ini
    infoMsg "::\n" \
    ":: diff docker.inie docker.ini\n" \
    "::"
    nbsp
    infoMsg "-+"
    diff -by -W100 --suppress-common-lines ./docker.inie ./docker.ini
    infoMsg "-+"
    nbsp

    rm -rf docker.inie

    # go back
    cd ../..

    # finish message
    logMsg "finished creator"
    endCounter=$(date +%s)
    totalTime=$(echo "$endCounter - $startCounter" | bc)
    echo " total time:" $(convertsecs $totalTime)
}

convertsecs() {
    h=`expr $1 / 3600`
    m=`expr $1  % 3600 / 60`
    s=`expr $1 % 60`
    printf "%02d:%02d:%02d\n" $h $m $s
}


warnMsg() {
    echo -e "" \
    $(tput setaf 3)::$(tput sgr 0) "\n"\
    $(tput setaf 3)$*$(tput sgr 0) "\n"\
    $(tput setaf 3)::$(tput sgr 0)
}

errorMsg() {
    echo -e "" \
    $(tput setaf 1)::$(tput sgr 0) "\n"\
    $(tput setaf 1)$*$(tput sgr 0) "\n"\
    $(tput setaf 1)::$(tput sgr 0)
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "$*"
}

nbsp() {
	echo ""
}

init